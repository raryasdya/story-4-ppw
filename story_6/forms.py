from django.forms import ModelForm
from .models import Activity, Person

class ActivityForm(ModelForm):
	required_css_class = 'required'
	class Meta:
		model = Activity
		fields = '__all__'

class PersonForm(ModelForm):
	class Meta:
		model = Person
		fields = ['nama']