from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .views import activity, add_activity, register
from .models import Activity, Person
from .apps import Story6Config

# Create your tests here.
class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(Story6Config.name, 'story_6')
		self.assertEqual(apps.get_app_config('story_6').name, 'story_6')

class TestActivity(TestCase):
	def test_activity_url_is_exist(self):
		response = Client().get('/activity/')
		self.assertEqual(response.status_code, 200)

	def test_activity_index_func(self):
		found = resolve('/activity/')
		self.assertEqual(found.func, activity)

	def test_activity_using_template(self):
		response = Client().get('/activity/')
		self.assertTemplateUsed(response, 'activity.html')

	def test_form_activity_works(self):
		aktivitas = Activity.objects.create(kegiatan="abc")
		orang = Person.objects.create(kegiatan=aktivitas, nama='abc')
		response = Client().post('/activity/', data={'id':1})
		self.assertEqual(response.status_code, 302)

	def test_activity_model_create_new_object(self):
		kegiatan = Activity(kegiatan="abc")
		kegiatan.save()
		self.assertEqual(Activity.objects.all().count(), 1)

class TestAddActivity(TestCase):
	def test_add_activity_url_is_exist(self):
		response = Client().get('/activity/add/')
		self.assertEqual(response.status_code, 200)

	def test_add_activity_index_func(self):
		found = resolve('/activity/add/')
		self.assertEqual(found.func, add_activity)

	def test_add_activity_using_template(self):
		response = Client().get('/activity/add/')
		self.assertTemplateUsed(response, 'add_activity.html')

	def test_form_activity_works(self):
		response = Client().post('/activity/add/', data={'kegiatan':'abc'})
		self.assertEqual(response.status_code, 302)

class TestRegist(TestCase):
	def setUp(self):
		kegiatan = Activity(kegiatan="abc")
		kegiatan.save()

	def test_regist_url_is_exist(self):
		response = Client().post('/activity/register/1/', data={'nama':'fairuza'})
		self.assertEqual(response.status_code, 302)

	def test_regist_using_template(self):
		response = Client().get('/activity/register/1/')
		self.assertTemplateUsed(response, 'register.html')