from django.shortcuts import render, redirect
from .models import Activity, Person
from .forms import ActivityForm, PersonForm

# Create your views here.
def activity(request):
	activity = Activity.objects.all()
	attendees = Person.objects.all()

	if request.method == "POST":
		Person.objects.get(id=request.POST['id']).delete()
		return redirect('/activity/')

	return render(request, 'activity.html', {'activity': activity, 'attendees': attendees})

def add_activity(request):
	if request.method == 'POST':
		form = ActivityForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/activity/')
	else:
		form = ActivityForm()
	return render(request, 'add_activity.html', {'form': form})

def register(request, index):
	if request.method == 'POST':
		form = PersonForm(request.POST)
		if form.is_valid():
			newPerson = Person(kegiatan=Activity.objects.get(id=index), nama=form.data['nama'])
			newPerson.save()
			return redirect('/activity/')
	else:
		form = PersonForm()
	return render(request, 'register.html', {'form': form})