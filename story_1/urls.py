from django.urls import path
from . import views

app_name = 'story_1'

urlpatterns = [
    path('story-1', views.home, name='home'),
]