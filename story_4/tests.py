from django.test import TestCase, Client
from django.urls import resolve
from .views import home, extra, time

# Create your tests here.
class TestHome(TestCase):
	def test_home_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_home_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, home)

	def test_home_using_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'homepage.html')

class TestExtra(TestCase):
	def test_extra_url_is_exist(self):
		response = Client().get('/extra/')
		self.assertEqual(response.status_code, 200)

	def test_extra_index_func(self):
		found = resolve('/extra/')
		self.assertEqual(found.func, extra)

	def test_extra_using_template(self):
		response = Client().get('/extra/')
		self.assertTemplateUsed(response, 'extra_page.html')