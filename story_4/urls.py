from django.urls import path
from . import views

app_name = 'story_4'

urlpatterns = [
    path('', views.home, name='home'),
    path('extra/', views.extra, name='extra'),
    path('time/', views.time, name='time'),
    path('time/<str:index>/', views.time, name='time'),
]