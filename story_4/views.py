from django.shortcuts import render
from django.utils.timezone import utc
import datetime

# Create your views here.
def home(request):
    return render(request, 'homepage.html')

def extra(request):
    return render(request, 'extra_page.html')

def time(request, index="0"):
	time_zone = (7 + int(index)) % 24
	utc_time = datetime.datetime.utcnow().replace(tzinfo=utc)
	timezone_time = datetime.datetime.utcnow() + datetime.timedelta(hours=time_zone)

	context= {
	    'timezone_time': timezone_time,
	    'utc_time': utc_time,
	    'utc': time_zone,
    }
	return render(request, 'time.html', context)