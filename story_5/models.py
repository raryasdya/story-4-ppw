from django.db import models

# Create your models here.
class Subject(models.Model):
	matkul = models.CharField('Subject', max_length=120, null=True)
	dosen = models.CharField('Lecturer', max_length=120, null=True)
	sks = models.IntegerField('Credit(s)', null=True)
	deskripsi = models.TextField('Description', blank=True)
	semester = models.CharField('Term', max_length=120, null=True, help_text='example : Odd 2019/2020')
	kelas = models.CharField('Classroom', max_length=120, null=True)

	def __str__(self):
		return self.matkul

class Task(models.Model):
	matkul = models.ForeignKey('Subject', on_delete=models.CASCADE, null=True)
	tugas = models.CharField('Task', max_length=120, null=True)
	deadline = models.DateField('Deadline', max_length=120, null=True)

	class Meta:
		ordering = ['deadline']