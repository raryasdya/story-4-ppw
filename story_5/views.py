from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Subject, Task
from .forms import SubjectForm
import datetime

# Create your views here.
def subject(request):
	if request.method == "POST":
		Subject.objects.get(id=request.POST['id']).delete()
		return redirect('/subject/')
	list_subject = Subject.objects.all()
	return render(request, 'subject_list.html', {'subject_list': list_subject})

def subject_detail(request, index):
	subject = Subject.objects.get(pk=index)
	task_list = Task.objects.filter(matkul_id=subject)

	overdue = task_list.filter(deadline__lt=datetime.date.today())
	near = task_list.filter(deadline__range=(datetime.date.today(), datetime.date.today() + datetime.timedelta(days=6)))
	far = task_list.filter(deadline__gte=datetime.date.today() + datetime.timedelta(days=7))

	context = {
		'subject' : subject,
		'overdue': overdue,
		'near' : near,
		'far' : far,
	}
	return render(request, 'subject_detail.html', context)

def add_subject(request):
	submitted = False
	if request.method == 'POST':
		form = SubjectForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/subject/add/?submitted=True')
	else:
		form = SubjectForm()
		if 'submitted' in request.GET:
			submitted = True
	return render(request, 'add_subject.html', {'form': form, 'submitted': submitted})