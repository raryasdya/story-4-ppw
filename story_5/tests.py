from django.test import TestCase, Client
from django.urls import resolve
from .views import subject, add_subject, subject_detail
from .models import Subject, Task

# Create your tests here.
class TestSubject(TestCase):
	def test_subject_url_is_exist(self):
		response = Client().get('/subject/')
		self.assertEqual(response.status_code, 200)

	def test_subject_index_func(self):
		found = resolve('/subject/')
		self.assertEqual(found.func, subject)

	def test_subject_using_template(self):
		response = Client().get('/subject/')
		self.assertTemplateUsed(response, 'subject_list.html')

class TestAddSubject(TestCase):
	def test_add_subject_url_is_exist(self):
		response = Client().get('/subject/add/')
		self.assertEqual(response.status_code, 200)

	def test_add_subject_index_func(self):
		found = resolve('/subject/add/')
		self.assertEqual(found.func, add_subject)

	def test_add_subject_using_template(self):
		response = Client().get('/subject/add/')
		self.assertTemplateUsed(response, 'add_subject.html')

class SubjectModelTest(TestCase):
	def subject_model_create_new_object(self):
		Subject.objects.create(matkul="abc", dosen="abc", sks=3, semester=2, kelas="A1.07")
		self.assertEqual(Subject.objects.all().count(), 1)

# class UnitTestDetailSubject(TestCase):
# 	def test_detail_subject_url_is_exist(self):
# 		response = Client().get('/subject/detail/<int:index>/')
# 		self.assertEqual(response.status_code, 200)

# 	def test_detail_subject_index_func(self):
# 		found = resolve('/subject/detail/<int:index>/')
# 		self.assertEqual(found.func, subject_detail)

# 	def test_detail_subject_using_template(self):
# 		response = Client().get('/subject/detail/<int:index>/')
# 		self.assertTemplateUsed(response, 'subject_detail.html')