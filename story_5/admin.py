from django.contrib import admin
from .models import Subject, Task

# Register your models here.
@admin.register(Subject)
class SubjectAdmin(admin.ModelAdmin):
	list_display = ('matkul', 'dosen', 'sks', 'semester', 'kelas', 'deskripsi')
	ordering = ('semester', 'matkul', 'dosen')
	search_fields = ('matkul', 'dosen', 'semester')

@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
	list_display = ('matkul', 'tugas', 'deadline')
	ordering = ('matkul', 'deadline')
	# search_fields = ('matkul', 'deadline', 'tugas')